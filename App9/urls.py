from django.urls import path
from . import views

app_name='App9'
   
urlpatterns = [
    path('', views.Index,name='Index'),
    path('books', views.books, name='books_data'),
    path('addLike', views.addLike, name='add_like'),
    path('topBook', views.topBook, name='top_book')
]