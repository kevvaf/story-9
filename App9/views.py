from django.shortcuts import render
from django.http import JsonResponse
import json
from .models import book
import requests
from django.views.decorators.csrf import csrf_exempt




def Index(request):
    
    return render(request, 'story9.html')

def books(request):
  
    q = request.GET['q']
    
    json_read = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + q)

    b=json_read.json()

    for item in b['items']:

	    try:
	        book = book.objects.get(id=item['id'])
	        likes = book.likes   

	    except:
	        likes = 0
	    item['volumeInfo']['likes'] = likes 

    return JsonResponse(b)

@csrf_exempt
def addLike(request):
    
    book, created = book.objects.get_or_create(
        id=request.POST['id'],
        cover=request.POST['cover'],
        title=request.POST['title'],
        author=request.POST['author'],
        
    )
    book.likes=int(request.POST['likes'])
    
    book.save()
    return JsonResponse(book.likes, safe=False)


def topBook(request):

	books= book.objects.order_by('-likes')[:5]
	library=[]
	for i in books:
		library.append({
			'id' : i.id,
			'cover': i.cover,
			'title': i.title,
			'author':i.author,
			'likes': i.likes
			})
	data={
		'top5' : library
	}
	return JsonResponse(data,safe=False)
	    





